
/* http://localhost:3000/app/dk/test/?tmpl=modal.tmpl#geodk.bright/11/9.8942/55.8451/ */
/**
 *
 * @type {*|exports|module.exports}
 */
var urlparser = require('./../../../browser/modules/urlparser');

/**
 *
 * @type {array}
 */
var urlVars = urlparser.urlVars;

/**
 * @type {string}
 */
var db = urlparser.db;

var sayHelloWorld;
var streetViewModule;
var vectorLayers;
var switchLayer;
var backboneEvents;

var layerGroup = [];
var horsensPopup = L.popup();
var meta;
var pointToLayerFunction;
var onEachFeatureFunction;
var cloud;
var map;
var filteredLayers;
var layers;

module.exports = {
    set: function(o){
        meta = o.meta;
        vectorLayers = o.extensions.vectorLayers.index;
        switchLayer = o.switchLayer;
        backboneEvents = o.backboneEvents;
        cloud = o.cloud;
        layers = o.layers;
    },
    init: function(){
        $('.areas').click(function(){
            var areaSelected = (parseInt($(this).attr("id")) - 1);
            
            vectorLayers.switchLayer("v:indsatsplaner.vandvaerk_view", true);

        });

        var map = cloud.get().map;
        //streetViewModule.sayHello();
        onEachFeatureFunction = function(feature, layer){
            //console.log(feature);
            if(feature.geometry.type == "MultiPoint" && feature.properties.navn){
                layer.bindPopup(
                    '<h3>'+feature.properties.navn+
                    '</h3> <hr> <p>'+feature.properties.beskrivelse+
                    '</p> <br> <a class="btn btn-default btn-sm teal darken-3 ml-0"  target="_blank" href="'+feature.properties.pdf+
                    '">Link</a>', {className: "horsens-popup"});
                    console.log('made it into oneachfeature', feature, layer);
            }
        };

        //vectorLayers.setOnEachFeature('v:indsatsplaner.vandvaerk_view', onEachFeatureFunction);

        pointToLayerFunction = function(feature, latlng){
            if(feature.properties.navn){
                
            }
            return L.marker(latlng, {
                icon: L.ExtraMarkers.icon({
                    icon: "",
                    shape: "circle",
                    markerColor: "blue-dark"
                })
            })
            
        };

        //vectorLayers.setPointToLayer('v:indsatsplaner.vandvaerk_view', pointToLayerFunction);

        $(".leaflet-top.leaflet-right").children().remove();

        backboneEvents.get().on("ready:vectorLayers", function () {
            var collectionOfLayers = meta.getMetaData();
            

            if(collectionOfLayers.data.length > 0){
                filteredLayers = collectionOfLayers.data.filter(function(value, index){
                    return value.f_table_schema == "indsatsplaner";
                });
                
            }

            filteredLayers.forEach(function(value, index){
                console.log(vectorLayers.getStores());
                var tempValue = 'v:'+value.f_table_schema + "." + value.f_table_name;
                //vectorLayers.switchLayer(tempValue, true);
                console.log(tempValue);
                vectorLayers.getStores()[tempValue].layer.options.onEachFeature = onEachFeatureFunction;
                vectorLayers.getStores()[tempValue].layer.options.pointToLayer = pointToLayerFunction;
                //console.log(vectorLayers.getStores()[tempValue].load());
                /*var promise1 = new Promise(function(resolve, reject){
                    resolve(vectorLayers.getStores()[tempValue].load());
                }).then(function(e){
                    console.log(e);
                });*/
                //console.log(vectorLayers.getStores()[tempValue]);
                vectorLayers.getStores()[tempValue].load();
                //vectorLayers.switchLayer(tempValue, true);
                //vectorLayer.addTo(map);
                var tempGroup = L.layerGroup();
                //vectorLayers.getStores()[tempValue].sql = "SELECT * FROM indsatsplaner.vandvaerk_view WHERE ID = '1'"
            });

            //vectorLayers.switchLayer('v:indsatsplaner.vandvaerk_view', true);
        });
    }
}