var express = require('express');
var path = require('path');
var app = express();
var bulk = require('bulk-require');
var bodyParser = require('body-parser');
var session = require('express-session');

app.use(bodyParser.json({
        limit: '50mb'
    })
);
// to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true,
    limit: '50mb'
}));

app.set('trust proxy', 1) // trust first proxy

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    name: "connect.gc2",
    cookie: { secure: false }
}));

app.use('/app/:db/:schema?', express.static(path.join(__dirname, 'public'), {maxage: '60s'}));

app.use('/', express.static(path.join(__dirname, 'public'),    {maxage: '1h'}));

app.use(require('./controllers'));

app.use(require('./extensions'));

app.enable('trust proxy');

var server = app.listen(3000, function () {
    console.log('Listening on port 3000...');
});

global.io = require('socket.io')(server);
io.on('connection', function (socket) {
    console.log(socket.id);
});